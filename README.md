DOMAIN LIBRARIES ATTACH
=======================


INTRODUCTION
------------

This module provides ability to attach different libraries of theme
to specific domains.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/domain_libraries_attach

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/domain_libraries_attach
  
  
REQUIREMENTS
------------

This module requires the following modules:
 * Domain (https://www.drupal.org/project/domain)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   

CONFIGURATION
-------------

 * Define library in `theme_name.libraries.yml`.
 * Go to `admin/config/domain/domain_libraries_attach`.
 * Assign library to domain that you need.


MAINTAINERS
-----------

Current maintainers:
 * Dima Koltovich (https://www.drupal.org/u/dkoltovich)
 * Dima Danylevskyi (https://www.drupal.org/u/danylevskyi)
